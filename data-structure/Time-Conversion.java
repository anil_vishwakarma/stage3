import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    /*
     * Complete the timeConversion function below.
     */
    static String timeConversion(String s) {
        String str[] =s.split(":");
       int h=Integer.parseInt(str[0]);String ho="";
        char sec[]=str[2].toCharArray();

        if(sec[2]=='P'){
            if(h<=11){
            h=12+h;
            ho=""+h;
            }
            else{
                ho=""+str[0];
            }
        } 
        else if(sec[2]=='A')
            {
                if(h==12){
                    ho="00";
                }
                else{
                ho=""+str[0];}
            }
            String q=""+sec[0]+sec[1];
            return (ho+":"+str[1]+":"+q);
    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scan.nextLine();

        String result = timeConversion(s);

        bw.write(result);
        bw.newLine();

        bw.close();
    }
}
